package com.atlassian.confluence.plugins.macros.basic;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.Map;

/**
 * @since 3.1.0
 */
public final class LoremIpsumBasicMacro extends com.atlassian.renderer.v2.macro.basic.LoremIpsumMacro implements Macro {

    public String execute(
            final Map<String, String> parameters,
            final String body,
            final ConversionContext context) throws MacroExecutionException {
        try {
            PageContext pageContext = (context == null) ? null : context.getPageContext();
            return execute(parameters, body, pageContext);
        } catch (MacroException e) {
            throw new MacroExecutionException(e);
        }
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

}
